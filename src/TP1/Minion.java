package TP1;

public abstract class Minion {

//Attributs
	//On cr�e un attribut nom de type String
	protected String nom;
	//On cr�e un attribut ann�e de type entier
	protected int annee = 0;
	//On cr�e un attribut bourse de type double
	protected double bourse=0;
	protected int retraite = 6 +(int)(Math.random() * ((9-6)+1)); //On d�finit un age de retraite entre 6 et 9ans


// On cr�e une m�thode pour pr�senter les stuarts
	public void sePresenter() {

		System.out.println(nom+" : j'ai "+bourse+" grosse(s) pi�ce(s) en bourse et "+retraite+" ann�es avant la retraite");
	}

	//On cr�e un accesseur pour r�cup�rer le nom
	public String getNom() {
		return this.nom;
	}
	
	//On cr�e un accesseur pour r�cup�rer la valeur de la bourse
	public double getBourse() {
		return bourse;
	}


	}	


