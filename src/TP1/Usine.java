package TP1;

import java.util.Vector;

public class Usine {

	//Attributs

	//On cr�e un vecteur liste qui va contenir des objets Stuart
	private Vector<Stuart> liste;
	//On cr�e un attribut nom de type String
	private String nom;
	//On cr�e un attribut nombreStuart de type entier
	private int nombreStuart;
	//On cr�e un attribut coffre de type double
	private double coffre;


	//On cr�e un constructeur Usine
	public Usine(String n) {
		this.nom=n;

		//On cr�e un nouveau vecteur pour la liste des Stuart
		liste = new Vector<Stuart>();


	}

	//On cr�e un accesseur pour faire retourner la valeur du nombre de Stuart
	public int nombredeStuart() {
		return nombreStuart;
	}

	//On cr�e une m�thode pour connaitre le nombre de Stuart
	public void SavoirnombreStuart() {
		System.out.println("Il y a "+nombredeStuart()+" dans l'usine");
	}

	//On cr�e un accesseur pour connaitre la somme contenu dans le coffre
	public double getCoffre() {
		return this.coffre;

	}
	//Methodes

	//Cette methode nous permettra de mettre le Stuart dans la list de l'usine
	public void ajouterdansusine(Stuart s) {
		liste.addElement(s);
		System.out.println(s.getNom()+ " est mis dans l'usine ");

	}

	//On cr�e une m�thode pour faire travailler son Stuart
	public void travaillerStuart(Stuart s) {
		s.travailler();	
		//Quand le Stuart part en retraite, on le supprime de l'usine
		liste.removeElement(s);
		//On enl�ve 20% de la bourse du Stuart � la fin de sa carri�re
		this.coffre = coffre +((s.bourse/100)*20);	
	}

	//M�thode pour faire travailler les Stuart et mettre des imp�ts
	public void travaillerStuartImpot() {
		for (int i=0; i<liste.size(); i++) {
			liste.elementAt(i).travaillerunefois();	
			this.coffre = coffre +((liste.elementAt(i).bourse/100)*20);	
			recupImpot();

			if (liste.elementAt(i).retraite == 0) {
				this.coffre = coffre + (liste.elementAt(i).bourse * 0.2);
				liste.removeElementAt(i);


			}
		}


	}

	//M�me m�thode que la pr�c�dente mais sans imp�t
	public void travaillerStuartSansImpot() {
		for (int i=0; i<liste.size(); i++) {
			liste.elementAt(i).travaillerunefois();	
			this.coffre = coffre +((liste.elementAt(i).bourse/100)*20);	

			if (liste.elementAt(i).retraite == 0) {
				this.coffre = coffre + (liste.elementAt(i).bourse * 0.2);
				liste.removeElementAt(i);

			}

		}


	}

	//M�thode pour r�cuperer 50% de la bourse des Stuart par an
	public void recupImpot() {
		for (int i = 0; i <liste.size(); i++) {
			System.out.println("On collecte les imp�ts de "+liste.elementAt(i).getNom());		
			double newbourse = liste.get(i).getBourse();
			double tax = newbourse*0.5;
			newbourse -= tax;
			coffre += tax;
		}

	}
	
	//M�thode pour ajouter un Stuart dans l'empire 
	public void ajouterdansusineEmpire(Stuart s) {
		liste.addElement(s);
		System.out.println(s.getNom()+ " est mis dans l'usine ");
		this.coffre= coffre - 50;

	}

}
