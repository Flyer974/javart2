package TP6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;


public class ChatClient extends JFrame implements ActionListener, Runnable {

	/*___________ Attributs ______________*/
	private JTextField messageAEnvoyer;		//Zone de saisie du message
	private JButton b_Envoyer;				//Le bouton d'envoi du message
	private Socket maSocket;				//Socket du programme client
	private int numeroPort = 8888;			//Port 
	private String adresseServeur = "localhost";//Adresse du serveur
	private PrintWriter writerClient;		//Objet permettant l'�criture de message sur le socket
	private BufferedReader readerClient;
	private JTextArea chat;

	/*___________ Constructeur ______________*/
	public ChatClient(){
		//D�finition de la fen�tre
		super("Client - Panneau d'affichage");
		setSize(300, 300);
		setLocation(300,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Cr�ation des composants graphiques
		messageAEnvoyer = new JTextField(20);

		chat = new JTextArea();

		JScrollPane scrollPane = new JScrollPane(chat, 
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		b_Envoyer = new JButton("Envoyer");
		b_Envoyer.addActionListener(this);

		//Disposition des composants graphiques
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		pane.add(scrollPane, BorderLayout.CENTER);
		pane.add(messageAEnvoyer, BorderLayout.NORTH);
		pane.add(b_Envoyer, BorderLayout.SOUTH);

		//Cr�ation du Socket client
		try {

			maSocket = new Socket(adresseServeur, numeroPort);
			writerClient = new PrintWriter(maSocket.getOutputStream()); 
		} catch (Exception e) {
			chat("Erreur Cr�ation client");
		}

		Thread t = new Thread(this);
		t.start();

		//Affichage de la fen�tre
		setVisible(true);
	}

	/*___________ M�thodes ______________*/
	@Override
	public void actionPerformed(ActionEvent e) {
		//Clic du bouton Envoyer d�clanche la m�thode emettre()
		if (e.getSource() == b_Envoyer) {
			emettre();
			//run();
		}

	}

	public void emettre() {
		//Envoi du message
		try {
			String message = messageAEnvoyer.getText(); 
			writerClient.println(message); //Envoi du texte par l'objet writer
			writerClient.flush();
			messageAEnvoyer.setText("");
			messageAEnvoyer.requestFocus();

		} catch (Exception e) {
			chat("Erreur Envoi du Message");
		}
	}



	/*___________ M�thode Main ______________*/
	public static void main (String[] args){
		new ChatClient();
	}

	public void run() {

		try {

			chat("Client connect�\n");
			//Les messages re�us sont format�s et stock�s dans une m�moire tampon 
			readerClient = new BufferedReader(new InputStreamReader(maSocket.getInputStream()));

			//On lit et affiche la chaine de caract�re
			String ligne; 
			while ((ligne = readerClient.readLine()) != null){ 
				chat("Vous : " + ligne + "\n");

			}

		} catch (Exception e) {
			chat("Impossible de recevoir le message");
		}

	}

	public void chat(String str){
		chat.append(str);
	}

}
