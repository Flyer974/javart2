package TP5;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import TP1.Stuart;

public class Tierce extends JFrame implements ActionListener, JugeDeCourse{

	//Attributs
	//On cr�e un panneau qui contiendra toute la fenetre
	private Container panneau;
	//On cr�e un Jbutton pour le bouton next
	private JButton GO;
	//On cr�e un label qui calculera le nombre de tours
	private JLabel result;
	private Vector<Cheval> liste;
	private Vector<JProgressBar>bar;



	//Constructeur
	public Tierce(){
		//Cr�ation de la fen�tre
		this.setTitle("Tierc�");
		this.setSize(300, 250);
		//On d�finit la location de la fenetre
		this.setLocation(300,200);
		//Cela va permettre de terminer l'application  la fermeture de la fentre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			//On cr�e un nouveau panel qui contiendra la piste
			JPanel piste = new JPanel(new GridLayout(6,1));

			//On cr�e un nouveau vecteur bar, qui contiendra les barres de progression
			bar = new Vector<JProgressBar>();

			//On ajoute une condition qui remplira les barres de progression
		for (int i =0; i< 6; i++) {
			bar.addElement(new JProgressBar());
			bar.elementAt(i).setMaximum(50);
			bar.elementAt(i).setMinimum(0);
			bar.elementAt(i).setValue(0);
			bar.elementAt(i).setStringPainted(true);
			piste.add(bar.elementAt(i));
		}

		//R�cup�ration du container
		panneau = getContentPane();


		//Cr�ation des objets � utiliser
		result = new JLabel("R�sultat");
		GO = new JButton("Go");


		//On ajoute au panneau le r�sultat de la course, la piste et un bouton pour faire commencer la course
		panneau.add(result, BorderLayout.NORTH);
		panneau.add(piste, BorderLayout.CENTER);
		panneau.add(GO, BorderLayout.SOUTH);

		//On �coute le bouton GO pour d�clencher une action
		GO.addActionListener(this);

		setVisible(true);

		//On cr�e un nouveau vecteur qui contiendra la liste des chevaux
		liste = new Vector<Cheval>();

		//On ajoute une condition pour remplir la liste des chevaux
		for (int i =0; i< 6; i++) {
			//On ajoute des param�tre pour cr�e le cheval
			Cheval chevalliste = new Cheval(i, 50, this, bar.elementAt(i));
			//On ajoute le cheval � la liste des chevaux
			liste.addElement(chevalliste);
		}

	}

	public static void main(String[] args) {
		
		//On cr�e un objet Tierce 
		Tierce t = new Tierce();

	}

	//M�thode
	public void actionPerformed(ActionEvent e) {

		//Cr�ation des 6 chevaux
		if (e.getSource() == GO) {

			for (int i =0; i< 6; i++) {
				//On lance le thread en fonction des �l�ments compris dans i
				liste.elementAt(i).start();
			}
		}


	}

	//On affiche une phrase lorsqu'un cheval passe la ligne d'arriv�
	public synchronized void passeLaLigneDArrivee(int id) {
		String texte = result.getText();
		result.setText(texte+" "+id);
	}

}
