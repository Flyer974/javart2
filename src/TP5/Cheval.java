package TP5;

import javax.swing.JProgressBar;

public class Cheval extends Thread implements CoureurHippique{

	//Attributs
	private int id;
	private int longueur;
	private int distance=0;
	private JugeDeCourse juge;
	private JProgressBar bar;

	//On cr�e un constructeur avec diff�rents param�tres. Un param�tre pour l'id, une pour la longueur, un pour le juge de course et un pour le JProgressbar
	public Cheval(int i, int l,  JugeDeCourse j, JProgressBar b) {
		this.id = i;
		this.longueur = l;
		this.juge=j;
		this.bar = b;

	}
		
//M�thode
	
	//On cr�e un asscceur pour retourner la valeur de la distance
	public int distanceParcourue() {
		return this.distance;
	}


	
	public void run() {
		while(distance < longueur) {
			//On fait en sorte que la valeur de la distance soit comprise  entre 3 et 0 au harsard
			distance= distance + (0 + (int)(Math.random() * ((3 - 0) + 1)));
			System.out.println("Le cheval N�"+id+" a parcouru "+distance+"m");
		}

		//On ajoute un try catch pour ajouter une exception
		try {
			Thread.sleep(50);
			bar.setValue(distance);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		//On ajoute une condition pour dire quand le cheval est arriv�
		if(distance>=longueur) {
			juge.passeLaLigneDArrivee(id);
			System.out.println("Le cheval N�"+id+" est arriv� ! Congrats");

		}
	}




}
