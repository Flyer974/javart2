package TP2_Java;

public abstract class Produit {
	//Attributs
	protected String nom;
	protected double prixAchat;
	protected double prixVente;
	protected double quantite;
	protected double supprimer;
	private String paysOrigine;
	protected double solde;
	protected double prixVenteInitiale;



	//On cr�e un constrcteur qui contiendra diff�rent attributs pour le nom, le prix d'achat, prix de vente et la quantit�
	public Produit(String n, double pa, double pv, double q) {
		prixAchat = pa;
		prixVente = pv;
		quantite = q;
		nom = n;	


	}

	//Accesseur

	//On un cr�e un assecceur getnom pour r�cup�rer le nom d'un produit
	public String getNom() {
		return this.nom;

	}

	//On cr�e un accesseur pour r�cup�rer le prix d'achat d'un produit
	public double getPrixAchat() {
		return this.prixAchat;
	}

	//On cr�e un acceseur pour r�cup�rer la quantit� d'un produit
	public double getQuantite() {
		return this.quantite;
	}

	//On cr�e un accesseur pour r�cup�rer le prix de vente d'un produit
	public double getPrixVente() {
		return this.prixVente;
	}

	//On cr�e un accesseur pour r�cup�rer la quantit� qu'on supprimer a un produit
	public double getSupprimer() {
		return this.supprimer;
	}

	//M�thode

	//On cr�e une m�thode pour pr�sente run produit
	public void presentation() {

	}

	//On cr�e une m�thode pour r�duire la quantit� d'un produit
	public void supprimerQuantite(double s) {
		this.supprimer = s;
		this.quantite = quantite - s;
		System.out.println("On supprimer "+getSupprimer()+ " "+getNom());
	}

	//On cr�e une m�thode pour augmenter le stock d'un produit
	public void ajouterStock(double r) {
		this.quantite = quantite + r;

	}

	//On cr�e une m�thode pour mettre un produit en solde
	public void vendreSolde(double s) {
		solde = (s/100);
		prixVenteInitiale=prixVente;
		System.out.print("Le prix de "+getNom()+" avant la solde est de "+getPrixVente());
		this.prixVente = this.prixVente - (this.prixVente * solde);
		System.out.print("\nLe prix de "+getNom()+" avec la solde est de "+getPrixVente());
	}

	//On cr�e une m�thode pour arr�ter la solde d'un produit
	public void stopSolde() {
		this.prixVente = prixVenteInitiale;
		System.out.println("\nLe prix pour "+getNom()+" est de "+getPrixVente()+" apr�s la fin des soldes");
	}




}
