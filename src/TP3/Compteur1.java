package TP3;

public class Compteur1 extends Thread {

	//Attribut
	private String nom;

	
	public Compteur1(String nom) {
		this.nom=nom;
	}


	public void run() {
		for (int i =1; i< 1000; i++) System.out.print(i + " " + nom );
	}

	public static void  main(String args[]) throws InterruptedException{
		Compteur1 t1, t2, t3;
		t1=new Compteur1("Hello ");
		t2=new Compteur1("World ");
		t3=new Compteur1("and Everybody ");

		t1.start();
		t2.start();
		t3.start();
		
		
		Thread.sleep(100);
		
		System.out.println("Fin du programme");
		
		System.exit(0);
		
		//Exercice 1.1 : Le noms d'affichage ne correspond pas au nombre voulu 
		//System.exit(0) va arr�ter tous les processus en cours, �galement les threads
		
		//Exercice 1.2 : Les 3 thread ont plus de temps pour afficher
		
		//L'affichage du fin de programmage d'affiche bien � la fin car les thread ont eu le temps d'afficher les noms
		

	}

}
