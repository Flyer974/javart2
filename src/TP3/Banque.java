package TP3;

public class Banque {

	public static void main(String[] args) {
		
		//Exercice 3.2
		System.out.println("Exercice 3.2");
		//On cr�e un nouveau objet c1 de type Compte
		Compte c1 = new Compte();
				
		//On utilise les m�thodes verserARgent et AfficherSolde
		c1.verserArgent(2);
		c1.afficherSolde();
		
		//Exerice 3.3
		System.out.println("\nExercice 3.3");
		//On cr�e un nouveau objet c2 de type compte
		Compte c2 = new Compte();
		
		//On cr�e un nouveau objet v1 de type VersePleinDargent 
		VerserPleinDArgent v1 = new VerserPleinDArgent(c2);
		//On cr�e un nouveau thread d'execution
		Thread my_thread= new Thread(v1);
		//On Execute le thread "my_thread"
		my_thread.start();
		
		//Exercice 3.4
		//On cr�e un nouveau objet v2 VersePleinDargent
		VerserPleinDArgent v2 = new VerserPleinDArgent(c2);
		//On cr�e un deuxi�me thread
		Thread my_thread2 = new Thread(v2);
		//On execute le deuxi�me Thread "my_thread2"
		my_thread2.start();
		
		
		
	}

}
